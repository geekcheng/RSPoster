#简单的消息订阅框架-RSPoster

##只需几行代码，完成消息订阅、发送
###1.初始化RSPoster:
	在Application初始化的时候：
		RSPoster.init();
###2.订阅消息:
	RSPoster.getInstance().like(Object msgType,RSMSGHandler handler);
###3.发送消息：
	 RSPoster.getInstance().post(new RSMessage() {
            @Override
            public Object getType() {
                return 1;
            }

            @Override
            public Object getMessageBody() {
                return "来自Activity的消息";
            }
        });
###4.收尾工作
	在接收器即将结束生命周期的时候，将其反注册
		RSPoster.getInstance().disLike(Object msgType,RSMSGHandler handler);
###5.其他
	停止发送消息：
		RSPoster.getInstance().stopPost();
	再次开启发送消息
		RSPoster.getInstance().startPost();
###Update
	1.增加方法
		//反注册该消息类型的所有Handler
		mRSposter.removeAll(msgType);
		mRsposter.removeAll(RSMSGHandler handler);
	