package so.raw.rsposter;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by zyw on 2015/9/15.
 */
public interface RSMSGHandler {
    //在主线程中相应消息
    public void doInMainThread(RSMessage msg);
    //在子线程中相应消息
    public void doInChildThread(RSMessage msg);
}
