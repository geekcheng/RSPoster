package so.raw.rsposter;

/**
 * Created by zyw on 2015/9/15.
 */

/**
 * 消息体，所有的消息都要继承这个接口
 */
public interface RSMessage<T,C>  {
    //获取消息类型
    T getType();
    //获取消息的消息体
    public C getMessageBody();
}
