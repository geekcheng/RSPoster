package so.raw.rsposter;

import android.os.Handler;
import android.util.Log;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by zyw on 2015/9/15.
 * 投递员，投递消息
 */
public class RSPoster {
    private static LinkedHashMap<Object,LinkedList<RSMSGHandler>> map; //消息类型->处理器 的列表
    private final RSMSGQueue mQueue = new RSMSGQueue(); //消息队列对象
    private PosterThread mPosterThread; // 轮询消息的队列
    private Handler mMainHandler; // 主Handler
    private static boolean mRunningFlag; //停止标记true = 继续运行，false = 停止
    private RSPoster(){
        map = new LinkedHashMap<>(); //初始化消息类型->处理器列表
        mMainHandler = new Handler(); //初始化handler
        mPosterThread = new PosterThread();// 初始化轮询器
        startPost();
    }
    //单例
    private static RSPoster instance = null;
    public static RSPoster init(){
        synchronized (RSPoster.class){
            if(instance == null){
                synchronized (RSPoster.class){
                    if(instance == null)
                        instance = new RSPoster();
                }
            }
        }
        return instance;
    }
    //获取实例对象
    public static RSPoster getInstance(){
        if(instance == null){
            init();
        }
        return instance;
    }

    /**
     * 关闭
     */
    public  void stopPost(){
        mRunningFlag = false;
    }

    /**
     * 启动
     */
    public  void startPost(){
        mRunningFlag = true;
        mPosterThread.start(); //启动轮询
    }
    /**
     * 订阅方法
     */
    public void like(Object msgType,RSMSGHandler handler){
        if(map.containsKey(msgType)){
            map.get(msgType).add(handler);
        }else{
            LinkedList<RSMSGHandler> handlers = new LinkedList<>();
            handlers.add(handler);
            map.put(msgType, handlers);
        }

    }

    /**
     * 取消订阅消息
     * @param msgType
     * @param handler
     */
    public void disLike(Object msgType,RSMSGHandler handler){
        try{
            map.get(msgType).remove(handler);
        }catch(Exception e){
            //TODO
        }
    }

    /**
     * 反注册该消息类型的所有接收器
     * @param msgType
     */
    public void removeAll(Object msgType){
        try{
            map.get(msgType).clear();
        }catch (Exception e){

        }
    }

    /**
     * 从所有订阅中反注册该接收器
     * @param handler
     */
    public void removeAll(RSMSGHandler handler){
        for(Map.Entry<Object, LinkedList<RSMSGHandler>> men : map.entrySet()){
            try{
                men.getValue().remove(handler);
            }catch (Exception e){

            }
        }

    }


    //轮询器
    private class PosterThread extends  Thread{
        @Override
        public void run() {
            while(mRunningFlag){
                if(mQueue.hasMore()){
                    dispatchEvent(mQueue.outQueue());
                }
            }
        }
        //分发消息
        private void dispatchEvent(RSMessage rsMessage) {
            LinkedList<RSMSGHandler> handlers = map.get(rsMessage.getType());
            if(handlers == null)
                return;
            if(handlers.size() == 0)
                return;
            for (RSMSGHandler handler: handlers){
                runInChildThread(new Runnable() {
                    @Override
                    public void run() {
                        handler.doInChildThread(rsMessage);
                    }
                });
                runOnMainThread(new Runnable() {
                    @Override
                    public void run() {
                        handler.doInMainThread(rsMessage);
                    }
                });
            }
        }
    }
    //子线程处理
    private void runInChildThread(Runnable runner){
        new Thread(){
            @Override
            public void run() {
                try{
                    runner.run();
                }catch (Exception e){
                    Log.e("RSPoster:ChildThread",e.getMessage());
                }
            }
        }.start();
    }
    //主线程处理
    private void runOnMainThread(Runnable runner){
        try{
            mMainHandler.post(runner);
        }catch (Exception e){
            Log.e("RSPoster:MainThread",e.getMessage());
        }
    }
    //发送消息
    public void post(RSMessage msg){
        mQueue.enQueue(msg);
    }
}
