package so.raw.rsposter;

import java.util.LinkedList;

/**
 * Created by zyw on 2015/9/15.
 * 消息队列，需要不断轮询处理
 */
public class RSMSGQueue {

    private LinkedList<RSMessage> mList;
    public RSMSGQueue(){
        mList = new LinkedList<>();
    }
    public boolean hasMore(){
        return !mList.isEmpty();
    }
    public void enQueue(RSMessage msg){
        mList.addLast(msg);
    }
    public RSMessage outQueue(){
        return mList.removeFirst();
    }

}
